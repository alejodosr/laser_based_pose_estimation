//////////////////////////////////////////////////////
///  DroneLaserBasedPoseEstimationROSModule.h
///
///  Created on: May 31, 2016
///      Author: alejodosr
///
///  Last modification on: May 31, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////


//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3Stamped.h>
#include<geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>


//Ground Robots: Messages out
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneSpeeds.h>

#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <nav_msgs/Odometry.h>

#include "tf/transform_datatypes.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"

#include "sensor_msgs/LaserScan.h"
#include "GRANSAC.h"
#include "LineModel.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "gazebo_msgs/ModelStates.h"

//#include <droneMsgsROS/vectorTargetsInImageStamped.h>


#define M_PI                3.14159265358979323846
#define M_HALF_PI           3.14159265358979323846/2
#define M_THREE_HALF_PI     3*3.14159265358979323846/2
#define FORTY_FIVE_IN_RAD   3.14159265358979323846/4
#define DIFFERENCE_MIN      0.5
#define DOOR_SIZE_MAX       1.26
#define DOOR_SIZE_MIN       1.14
#define WINDOW_SIZE_MAX     1.1
#define WINDOW_SIZE_MIN     0.9
#define FACADE_TOLERANCE    0.3
#define DIFF_POSE_TOLERANCE 0.4     // Tolerance between to consecutive poses
#define MAX_PEAKS			10
//#define RIGHT_DOOR_POSE_X   12.501615
//#define RIGHT_DOOR_POSE_Y   2.147260
//#define RIGHT_WIN_POSE_X    12.494354
//#define RIGHT_WIN_POSE_Y    -3.603684
#define RIGHT_DOOR_POSE_X   0
#define RIGHT_DOOR_POSE_Y   0
#define RIGHT_WIN_POSE_X    0
#define RIGHT_WIN_POSE_Y    -8
#define MIN_ANGLE           -M_PI/4
#define MAX_ANGLE           M_PI/4
#define RANSAC_THRESHOLD    0.2
#define RANSAC_NITERATIONS  50
#define LIDAR_MAX_RANGE     30
#define BUFFER_SIZE         10
#define WINDOW_SMA_SIZE     5
//#define SHOW_LINES              // Comment this flag if you do not want to show the facade and the fitted line
#define ERROR_IN_SIM        0   // Disable this flag if you do not hace Gazebo Ground Truth



/////////////////////////////////////////
// Class DroneLaserBasedPoseEstimationROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneLaserBasedPoseEstimationROSModule : public DroneModule
{
public:

    /// Position controller members
    ///
    geometry_msgs::Vector3Stamped rotation_angles;

    //subscribers
protected:
    //Subscriber
    ros::Subscriber droneRotAngSub;
    ros::Subscriber droneScanSub;
    ros::Subscriber droneOdomFilteredSub;

    //Publisher
    ros::Publisher droneOdomPub;
    ros::Publisher droneDoorPub;
    ros::Publisher droneWindowPub;

    double window_x_global;
    double window_y_global;
    double door_x_global;
    double door_y_global;

    std::vector<double> door_x_relatives, door_y_relatives;
    double door_x_relative_prev, door_y_relative_prev;
		int consecutive_peak;

    double odom_yaw;

		ros::Time prev_time;

    #if ERROR_IN_SIM == 1
    // Error members
    int num_door_iterations;
    double door_avg_error_x, door_avg_error_y, door_max_error_x, door_min_error_x, door_max_error_y, door_min_error_y;

    int num_window_iterations;
    double window_avg_error_x, window_avg_error_y, window_max_error_x, window_min_error_x, window_max_error_y, window_min_error_y;

    double gt_x, gt_y;

    std::vector<double> yaw_values;

    void droneModelStatesCallback(gazebo_msgs::ModelStatesConstPtr msg);

    #endif

//    void droneVelCallback(geometry_msgs::Twist::ConstPtr msg);
    void droneRotCallback(geometry_msgs::Vector3Stamped::ConstPtr msg);
    void droneScanCallback(sensor_msgs::LaserScan::ConstPtr msg);
    void droneOdometryFilteredCallback(const nav_msgs::Odometry &msg);
    bool isOnTheLine(double point, double facade_tolerance, std::vector<double> y_line);
    template <typename T>
    std::vector<T> SimpleMovingAverage(std::vector<T> traj, int window_size);

public:
    DroneLaserBasedPoseEstimationROSModule();
    ~DroneLaserBasedPoseEstimationROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};


