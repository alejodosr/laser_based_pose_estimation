//////////////////////////////////////////////////////
///  droneLaserBasedPoseEstimationROSModuleSource.cpp
///
///  Created on: May 31, 2016
///      Author: alejodosr
///
///  Last modification on: May 31, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////


#include "droneLaserBasedPoseEstimationROSModuleHeader.h"

using namespace std;

DroneLaserBasedPoseEstimationROSModule::DroneLaserBasedPoseEstimationROSModule() : DroneModule(droneModule::active)
{

    return;
}


DroneLaserBasedPoseEstimationROSModule::~DroneLaserBasedPoseEstimationROSModule()
{
    close();
    return;
}

bool DroneLaserBasedPoseEstimationROSModule::init()
{
    DroneModule::init();

    return true;
}


void DroneLaserBasedPoseEstimationROSModule::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }

    //// TOPICS
    //Subscribers
    droneScanSub = n.subscribe("scan", 1, &DroneLaserBasedPoseEstimationROSModule::droneScanCallback, this);
    droneRotAngSub = n.subscribe("rotation_angles", 1, &DroneLaserBasedPoseEstimationROSModule::droneRotCallback, this);
    droneOdomFilteredSub        = n.subscribe("odometry/filtered",1,&DroneLaserBasedPoseEstimationROSModule::droneOdometryFilteredCallback, this);

#if ERROR_IN_SIM == 1
    droneRotAngSub = n.subscribe("/gazebo/model_states", 1, &DroneLaserBasedPoseEstimationROSModule::droneModelStatesCallback, this);
#endif

    //Publishers
    droneOdomPub=n.advertise<nav_msgs::Odometry>("/odom", 1, true);
    droneDoorPub = n.advertise< nav_msgs::Odometry>("lb_estimated_pose/door_pose", 1000);
    droneWindowPub = n.advertise< nav_msgs::Odometry>("lb_estimated_pose/window_pose", 1000);


    //Flag of module opened
    droneModuleOpened=true;

    //autostart module!
    //moduleStarted=true;

    prev_time = ros::Time::now();
    door_x_relative_prev = 1E9;
    door_y_relative_prev = 1E9;
    consecutive_peak = 0;

#if ERROR_IN_SIM == 1

    // Error variables initialization
    num_door_iterations = 0;
    door_avg_error_x = 0;
    door_avg_error_y = 0;
    door_max_error_x = 0;
    door_min_error_x = 1E6;
    door_max_error_y = 0;
    door_min_error_y = 1E6;

    num_window_iterations = 0;
    window_avg_error_x = 0;
    window_avg_error_y = 0;
    window_max_error_x = 0;
    window_min_error_x = 1E6;
    window_max_error_y = 0;
    window_min_error_y = 1E6;

#endif

    //End
    return;
}

void DroneLaserBasedPoseEstimationROSModule::droneOdometryFilteredCallback(const nav_msgs::Odometry &msg)
{
    if(moduleStarted){
        tf::Quaternion q(msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);
        tf::Matrix3x3 m(q);

        //convert quaternion to euler angels
        double r,p;
        m.getEulerYPR(odom_yaw, p, r);

//        cout << "ODOM YAW: " << odom_yaw << endl;
    }
}

template <typename T>
std::vector<T> DroneLaserBasedPoseEstimationROSModule::SimpleMovingAverage(std::vector<T> traj, int window_size){

    // Variables definition
    vector<T> moving_window;
    vector<T> out_traj;

    if (window_size < traj.size()){
        // Initialize the moving window
        for (int i=0; i < window_size; i++){
            moving_window.push_back((T) 0);
        }

        for (int i=0; i < traj.size(); i++){
            // Shift moving window
            for(int j=1; j < moving_window.size(); j++){
                moving_window[j-1] = moving_window[j];
            }
            moving_window[moving_window.size() - 1] = traj[i];

            // Calculate average
            double avg = 0;
            for(int j=0; j < moving_window.size(); j++){
                avg += moving_window[j];
            }
            avg /= moving_window.size();
            out_traj.push_back(avg);
        }

        // Return the sma vector
        return out_traj;
    }
    else{
        // Return the sma vector
        return traj;
    }
}


bool DroneLaserBasedPoseEstimationROSModule::isOnTheLine(double point, double facade_tolerance, vector<double> y_line){
    // Check in the whole line if the point meets the maximum tolerance
//    double min = 1E9;
    for(int i=0; i < y_line.size(); i++){
        if(abs(y_line[i] - point) < (double) facade_tolerance){
//            cout << "ES MENOR min distance in line: " << min << "\tThe point to compare: " << point << endl;
            return true;
        }
//        if (abs(y_line[i] - point) < min)   min = abs(y_line[i] - point);
    }
//    cout << "min distance in line: " << min << "\tThe point to compare: " << point << endl;
    return false;
}

void DroneLaserBasedPoseEstimationROSModule::droneScanCallback(sensor_msgs::LaserScan::ConstPtr msg){
    if(moduleStarted){
        /// Estimate frequency
        ros::Time time = ros::Time::now();
        // double frequency = 1 / ( (time.sec - prev_time.sec) + (ros::Time::now().nsec - prev_time.nsec) / (double) 1E9);
        ros::Duration  diff = (time - prev_time);
        double frequency = 1 / diff.toSec();
        prev_time = time;
//        cout << "My frequency is: " << frequency << endl;

        //if(0){
        /// Variables declaration
        ///
        double angle_min = msg -> angle_min;
        double angle_max = msg -> angle_max;
        double angle_increment = msg -> angle_increment;

        double range_min = msg -> range_min;
        double range_max = msg -> range_max;

        /// Filling angles vector
        ///
        double angle = 0;
        int i = 0;
        vector<double> angles, angles_filtered, ranges_filtered;
        while(angle <= angle_max){
            angle = angle_min + i * angle_increment;
            angles.push_back(angle);
            /// Filtering angles. The facade is the point to look (other measures are noise)
            ///
            if((angle > MIN_ANGLE) && (angle < MAX_ANGLE)){ // degrees of field of view (enough to see the facade)
                angles_filtered.push_back(angle);
                if (msg -> ranges[i] > LIDAR_MAX_RANGE) ranges_filtered.push_back(LIDAR_MAX_RANGE);
                else ranges_filtered.push_back(msg -> ranges[i]);
                //            cout << angles_filtered[i] << angles_filtered[i] << endl;
            }
            i++;
        }
        //    cout << "range size: " << ranges_filtered.size() << "  angles size: " << angles_filtered.size() << endl;

        /// Cartesian switching from Polar
        ///
        vector<double> x_filtered, y_filtered;
        double min_x = 1E15;
        double min_y = 1E15;
        for(i = 0; i < ranges_filtered.size(); i++){
            x_filtered.push_back(ranges_filtered[i] * sin(angles_filtered[i]));
            y_filtered.push_back(ranges_filtered[i] * cos(angles_filtered[i]));
            //        cout << x_filtered[i] << "   " << y_filtered[i] << endl;
            if (x_filtered.back() < min_x)  min_x = x_filtered.back();
            if (y_filtered.back() < min_y)  min_y = y_filtered.back();
        }

        //    cout << min_x << "  " << min_y << endl;

        // Moving it to the first quadrant (starting from 0)
        for(i = 0; i < x_filtered.size(); i++){
            x_filtered[i] -= min_x;
            y_filtered[i] -= min_y;
            //                    cout << x_filtered[i] << "   " << y_filtered[i] << endl;
        }

        /// Line fitting with RANSAC
        ///
        int Side = 72;
        cv::Mat Canvas(Side, Side, CV_8UC3);
        Canvas.setTo(255);
        // Fill the vector with the points
        double nPoints = x_filtered.size();
        std::vector<std::shared_ptr<GRANSAC::AbstractParameter>> CandPoints;
        for(int i = 0; i < nPoints; ++i)
        {
            cv::Point2d Pt(x_filtered[i], y_filtered[i]);
#ifdef SHOW_LINES
            cv::circle(Canvas, Pt, 0, cv::Scalar(0, 0, 0), -1);
#endif

            std::shared_ptr<GRANSAC::AbstractParameter> CandPt = std::make_shared<Point2D>(Pt.x, Pt.y);
            CandPoints.push_back(CandPt);
        }

#ifdef SHOW_LINES
        cv::imshow("RANSAC Facade", Canvas);
        //        char Key = cv::waitKey(1);
#endif

        // Estimate line
        GRANSAC::RANSAC<Line2DModel, 2> Estimator;
        Estimator.Initialize(RANSAC_THRESHOLD, RANSAC_NITERATIONS); // Threshold, iterations
        Estimator.Estimate(CandPoints);

        auto BestLine = Estimator.GetBestModel();
        if(BestLine)
        {
            auto BestLinePt1 = std::dynamic_pointer_cast<Point2D>(BestLine->GetModelParams()[0]);
            auto BestLinePt2 = std::dynamic_pointer_cast<Point2D>(BestLine->GetModelParams()[1]);
            if(BestLinePt1 && BestLinePt2)
            {
                double pt1_x,pt1_y, pt2_x, pt2_y, m, o;

#ifdef SHOW_LINES
                // Get the line in OpenCV coordinates
                pt1_x = BestLinePt1->m_Point2D[0];
                pt1_y = BestLinePt1->m_Point2D[1];
                pt2_x = BestLinePt2->m_Point2D[0];
                pt2_y = BestLinePt2->m_Point2D[1];

                m = (pt2_y - pt1_y) / (pt2_x - pt1_x);
                o = pt1_y - m * pt1_x;

                //            cout << "m: " << m << "  o: " << o << endl;


                // Draw the line in CV
                cv::Point Pt1(BestLinePt1->m_Point2D[0], BestLinePt1->m_Point2D[1]);
                cv::Point Pt2(BestLinePt2->m_Point2D[0], BestLinePt2->m_Point2D[1]);
                cv::Point p(0,0), q(Canvas.cols, Canvas.rows);

                p.y = -(Pt1.x - p.x) * m + Pt1.y;
                q.y = -(Pt2.x - q.x) * m + Pt2.y;


                cv::line(Canvas, p, q, cv::Scalar(0, 0, 255), 2, 8, 0);
                cv::imshow("RANSAC Facade", Canvas);
                char Key = cv::waitKey(1);
#endif

                // Get the line parameters
                pt1_x = BestLinePt1->m_Point2D[0] + min_x;
                pt1_y = BestLinePt1->m_Point2D[1] + min_y;
                pt2_x = BestLinePt2->m_Point2D[0] + min_x;
                pt2_y = BestLinePt2->m_Point2D[1] + min_y;

                m = (pt2_y - pt1_y) / (pt2_x - pt1_x);
                o = pt1_y - m * pt1_x;

//                cout << "m: " << m << "  o: " << o << endl;

                /// Select window and door
                ///

                // Get abrupt differences in distances
                vector<int> abrupt_filtered;
                double difference_filtered;
                for(i = 0; i < ranges_filtered.size() - 1; i++){
                    difference_filtered = ranges_filtered[i + 1] - ranges_filtered[i];
                    if (abs(difference_filtered) >= DIFFERENCE_MIN){
                        //            cout << "difference: " << difference << endl;
                        abrupt_filtered.push_back(i);
                    }
                }

                // Save the line of the facade in a vector
                vector<double> y_line;
                for (int i = 0; i < x_filtered.size(); i++){
                    y_line.push_back(m * (x_filtered[i] + min_x) + o);
                    //                cout << "y_line: " << y_line[i] << endl;
                }

                // Calculate the yaw from the slope of the facade
                double dot = y_line[0] - y_line[1];
                double det = x_filtered[0] - x_filtered[1];
                double yaw = atan2(det, dot);

                // Adjust the zero value
                if(yaw < 0) yaw += M_PI / 2;
                else    yaw -= M_PI / 2;

                // Meet the AeroStack standards
                yaw *= -1;

                //            if(yaw < 0) yaw += 2 * M_PI;
                //            yaw-= M_PI;

//                cout << "YAW: " << yaw << endl;

                // Get the openings
                double opening_size;
                double door_size = 0, door_angle = 0, door_range = 0, door_angle_left = 0, door_range_left = 0;
                double window_size = 0, window_angle = 0, window_range = 0;

                // Debug
                vector<double> opening_vector;

                for (i = 0; i < abrupt_filtered.size(); i++){
                    for(int j = i + 1; j < abrupt_filtered.size(); j++){
                        opening_size = sqrt(
                                    ranges_filtered[abrupt_filtered[i]] * ranges_filtered[abrupt_filtered[i]] +
                                ranges_filtered[abrupt_filtered[j] + 1] * ranges_filtered[abrupt_filtered[j] + 1] -
                                2 * ranges_filtered[abrupt_filtered[i]] *
                                ranges_filtered[abrupt_filtered[j]+ 1] * cos(angles_filtered[abrupt_filtered[j] + 1] - angles_filtered[abrupt_filtered[i]])
                                );
                        //                    cout << abs( (ranges_filtered[abrupt_filtered[i]] * cos(angles_filtered[abrupt_filtered[i]])) - (m * x_filtered[abrupt_filtered[i]] + o) )  << "   "
                        //                        << abs(ranges_filtered[abrupt_filtered[j] + 1] * cos(angles_filtered[abrupt_filtered[j] + 1]) - (m * x_filtered[abrupt_filtered[j] + 1] + o)) << endl;
                        // cout << "opening_size: " << opening_size << endl;
                        opening_vector.push_back(opening_size);

                        if ((opening_size < DOOR_SIZE_MAX) && (opening_size > DOOR_SIZE_MIN)){  // Bounds to localize the door
                            //                        if ( abs( (ranges_filtered[abrupt_filtered[i]] * cos(angles_filtered[abrupt_filtered[i]])) - (m * x_filtered[abrupt_filtered[i]] + o) )
                            //                             < FACADE_TOLERANCE &&
                            //                                ( abs(ranges_filtered[abrupt_filtered[j] + 1] * cos(angles_filtered[abrupt_filtered[j] + 1]) - (m * x_filtered[abrupt_filtered[j] + 1] + o))
                            //                                  < FACADE_TOLERANCE)){ // Points should be on the facade
                            if ( isOnTheLine(ranges_filtered[abrupt_filtered[i]] * abs(cos(angles_filtered[abrupt_filtered[i]])), (double) FACADE_TOLERANCE, y_line) &&
                                 isOnTheLine(ranges_filtered[abrupt_filtered[j] + 1] * abs(cos(angles_filtered[abrupt_filtered[j] + 1])), (double) FACADE_TOLERANCE, y_line)
                                 ){ // Points should be on the facade
                                door_size = opening_size;
                                door_angle = angles_filtered[abrupt_filtered[i]];
                                door_range = ranges_filtered[abrupt_filtered[i]];
                                door_angle_left = angles_filtered[abrupt_filtered[j] + 1];
                                door_range_left = ranges_filtered[abrupt_filtered[j] + 1];
                                //                            cout << "door size: " << door_size << endl;
                            }
                        }
                        if ((opening_size < WINDOW_SIZE_MAX) && (opening_size > WINDOW_SIZE_MIN)){  // Bounds to localize the door
                            //                        if ( abs( (ranges_filtered[abrupt_filtered[i]] * cos(angles_filtered[abrupt_filtered[i]])) - (m * x_filtered[abrupt_filtered[i]] + o) )
                            //                             < FACADE_TOLERANCE &&
                            //                                ( abs(ranges_filtered[abrupt_filtered[j] + 1] * cos(angles_filtered[abrupt_filtered[j] + 1]) - (m * x_filtered[abrupt_filtered[j] + 1] + o))
                            //                                  < FACADE_TOLERANCE)){ // Points should be on the facade
                            if ( isOnTheLine(ranges_filtered[abrupt_filtered[i]] * abs(cos(angles_filtered[abrupt_filtered[i]])), (double) FACADE_TOLERANCE, y_line) &&
                                 isOnTheLine(ranges_filtered[abrupt_filtered[j] + 1] * abs(cos(angles_filtered[abrupt_filtered[j] + 1])), (double) FACADE_TOLERANCE, y_line)
                                 ){ // Points should be on the facade
                                window_angle = angles_filtered[abrupt_filtered[i]];
                                window_range = ranges_filtered[abrupt_filtered[i]];
                                //                            cout << "window size: " << window_size << endl;

                            }
                        }
                    }
                }


                /// Estimate relative coordinates from door and window (X and Y axes)
                ///
                if (door_range != 0){
                    double door_x_relative = door_range * cos(door_angle /*+ odom_yaw*/ /*+ yaw */ /*+ yaw_values_filtered[yaw_values_filtered.size() - 1]*/) * cos(rotation_angles.vector.y);
                    double door_y_relative = door_range * sin(door_angle /* + odom_yaw */ /* + yaw */ /*+ yaw_values_filtered[yaw_values_filtered.size() - 1]*/) * cos(rotation_angles.vector.x);
                    double door_x_relative_left = door_range_left * cos(door_angle_left /*+ odom_yaw*/ /*+ yaw */ /*+ yaw_values_filtered[yaw_values_filtered.size() - 1]*/);
                    double door_y_relative_left = door_range_left * sin(door_angle_left /* + odom_yaw */ /* + yaw */ /*+ yaw_values_filtered[yaw_values_filtered.size() - 1]*/);

                    // Debug
//                    door_x_relative = door_x_relative_left;
//                    door_y_relative = door_y_relative_left;

                    // Compute avg between to measurements
                    door_x_relative = ( door_x_relative + door_x_relative_left ) / 2;
                    door_y_relative = ( door_y_relative - 1.2 + door_y_relative_left ) / 2;

                    if (consecutive_peak == MAX_PEAKS)	door_x_relative_prev = (double) 1E9;

                    // Initial state
                    if (door_x_relative_prev == (double) 1E9){
                        door_x_relative_prev = door_x_relative;
                        door_y_relative_prev = door_y_relative;
                        consecutive_peak = 0;
                        cout << "Initializing peaks.." << endl;
                    }

                    /// Discard high changes
                    double diff_x_pose = door_x_relative - door_x_relative_prev;
                    double diff_y_pose = door_y_relative - door_y_relative_prev;

                    if(abs(diff_x_pose) > DIFF_POSE_TOLERANCE || abs(diff_y_pose) > DIFF_POSE_TOLERANCE){
                        door_x_relative = door_x_relative_prev;
                        door_y_relative = door_y_relative_prev;
                        cout << "Discarding a peak.." << endl;
                        consecutive_peak++;

                        // Debug
                        for(int i=0; i< opening_vector.size(); i++) cout << "opening size: " << opening_vector[i] << endl;
                    }
                    else{
                        door_x_relative_prev = door_x_relative;
                        door_y_relative_prev = door_y_relative;
                    }

                    //                cout << "DOOR ANGLE: " << door_angle  << "YAW: " << yaw << endl;
                    //                cout << "door x: " << door_x_relative << "  door y: " << door_y_relative << endl;


                    /// Smooth the laser estimation to decrease the error
                    ///
                    if(door_x_relatives.size() < BUFFER_SIZE){
                        door_x_relatives.push_back(door_x_relative);
                    }
                    else{
                        // Shift the vector
                        for(int i = 0; i < door_x_relatives.size() - 1; i++){
                            door_x_relatives[i] = door_x_relatives[i + 1];
                        }
                        door_x_relatives[door_x_relatives.size() - 1] = door_x_relative;
                    }
                    vector<double> door_x_relatives_filtered = SimpleMovingAverage(door_x_relatives, WINDOW_SMA_SIZE);

                    if(door_y_relatives.size() < BUFFER_SIZE){
                        door_y_relatives.push_back(door_y_relative);
                    }
                    else{
                        // Shift the vector
                        for(int i = 0; i < door_y_relatives.size() - 1; i++){
                            door_y_relatives[i] = door_y_relatives[i + 1];
                        }
                        door_y_relatives[door_y_relatives.size() - 1] = door_y_relative;
                    }
                    vector<double> door_y_relatives_filtered = SimpleMovingAverage(door_y_relatives, WINDOW_SMA_SIZE);

                    /// Compute the transformation to Global Coordinates
                    ///
                    door_x_global = (-1) * (door_x_relatives_filtered[door_x_relatives_filtered.size() - 1] - RIGHT_DOOR_POSE_X);
                    door_y_global = (-1) * (door_y_relatives_filtered[door_y_relatives_filtered.size() - 1] - RIGHT_DOOR_POSE_Y);

                    //                cout << "global x (door): " << door_x_global << "  global y (door): " << door_y_global << endl;

                    // Publish the pose
                    nav_msgs::Odometry pose;
                    pose.header.stamp = ros::Time::now();
                    //               pose.pose.pose.position.x = door_poses_x_filtered[door_poses_x_filtered.size() - 1];
                    pose.pose.pose.position.x = door_x_global;
                    pose.pose.pose.position.y = door_y_global;
                    pose.pose.pose.position.z = yaw;

                    tf::Quaternion q = tf::createQuaternionFromRPY(0, 0, yaw);

                    pose.pose.pose.orientation.x = q.getX();
                    pose.pose.pose.orientation.y = q.getY();
                    pose.pose.pose.orientation.z = q.getZ();
                    pose.pose.pose.orientation.w = q.getW();

                    droneDoorPub.publish(pose);

#if ERROR_IN_SIM == 1

                    /// Errors computation
                    ///
                    num_door_iterations++;

                    // Errors from the door
                    cout << "----------- Door Errors -------------" << endl;

                    double door_inst_error_x = door_x_global - gt_x;
                    double door_inst_error_y = door_y_global - gt_y;

                    cout << "Inst. error:     x: " << door_inst_error_x << "    y: " << door_inst_error_y << endl;

                    door_avg_error_x *= (num_door_iterations - 1);
                    door_avg_error_x += door_inst_error_x;
                    door_avg_error_x /= num_door_iterations;

                    door_avg_error_y *= (num_door_iterations - 1);
                    door_avg_error_y += door_inst_error_y;
                    door_avg_error_y /= num_door_iterations;

                    cout << "Avg error:     x: " << door_avg_error_x << "    y: " << door_avg_error_y << endl;

                    if (abs(door_inst_error_x) > door_max_error_x)   door_max_error_x = door_inst_error_x;
                    if (abs(door_inst_error_x) < door_min_error_x)   door_min_error_x = door_inst_error_x;

                    cout << "Max error:     x: " << door_max_error_x << "    y: " << door_max_error_y << endl;

                    if (abs(door_inst_error_y) > door_max_error_y)   door_max_error_y = abs(door_inst_error_y);
                    if (abs(door_inst_error_y) < door_min_error_y)   door_min_error_y = abs(door_inst_error_y);

                    cout << "Min error:     x: " << door_min_error_x << "    y: " << door_min_error_y << endl;

#endif

                }

                if (window_range != 0){
                    double window_x_relative = window_range * cos(window_angle/* + yaw*/);
                    double window_y_relative = window_range * sin(window_angle/* + yaw*/);

                    // Take into account roll and pitch
                    window_x_relative = window_x_relative * cos(rotation_angles.vector.y);
                    window_y_relative = window_y_relative * cos(rotation_angles.vector.x);

                    //                cout << "window x: " << window_x_relative << "  window y: " << window_y_relative << endl;

                    /// Compute the transformation to Global Coordinates
                    ///
                    window_x_global = (-1) * (window_x_relative - RIGHT_WIN_POSE_X);
                    window_y_global = (-1) * (window_y_relative - RIGHT_WIN_POSE_Y);

                    //                cout << "global x (win): " << window_x_global << "  global y (win): " << window_y_global << endl;

                    // Publish the pose
                    nav_msgs::Odometry pose;
                    pose.header.stamp = ros::Time::now();
                    pose.pose.pose.position.x = window_x_global;
                    pose.pose.pose.position.y = window_y_global;

                    droneWindowPub.publish(pose);

#if ERROR_IN_SIM == 1

                    /// Errors computation
                    ///
                    num_window_iterations++;

                    // Errors from the winow
                    cout << "----------- Window Errors -------------" << endl;

                    double window_inst_error_x = window_x_global - gt_x;
                    double window_inst_error_y = window_y_global - gt_y;

                    cout << "Inst. error:     x: " << window_inst_error_x << "    y: " << window_inst_error_y << endl;

                    window_avg_error_x *= (num_window_iterations - 1);
                    window_avg_error_x += window_inst_error_x;
                    window_avg_error_x /= num_window_iterations;

                    window_avg_error_y *= (num_window_iterations - 1);
                    window_avg_error_y += window_inst_error_y;
                    window_avg_error_y /= num_window_iterations;

                    cout << "Avg error:     x: " << window_avg_error_x << "    y: " << window_avg_error_y << endl;

                    if (abs(window_inst_error_x) > window_max_error_x)   window_max_error_x = abs(window_inst_error_x);
                    if (abs(window_inst_error_x) < window_min_error_x)   window_min_error_x = abs(window_inst_error_x);

                    cout << "Max error:     x: " << window_max_error_x << "    y: " << window_max_error_y << endl;

                    if (abs(window_inst_error_y) > window_max_error_y)   window_max_error_y = abs(window_inst_error_y);
                    if (abs(window_inst_error_y) < window_min_error_y)   window_min_error_y = abs(window_inst_error_y);

                    cout << "Min error:     x: " << window_min_error_x << "    y: " << window_min_error_y << endl;

#endif

                }




            }
        }
        //}
    }
}

void DroneLaserBasedPoseEstimationROSModule::droneRotCallback(geometry_msgs::Vector3Stamped::ConstPtr msg){
    if(moduleStarted){
        rotation_angles = *msg;

        // Converting to radians
        rotation_angles.vector.x = rotation_angles.vector.x * M_PI / 180;
        rotation_angles.vector.y = rotation_angles.vector.y * M_PI / 180;
        rotation_angles.vector.z = rotation_angles.vector.z * M_PI / 180;

        //    // Adjusting angles to fit our calculations
        //    if (rotation_angles.vector.z < 0)  rotation_angles.vector.z = 2 * M_PI + rotation_angles.vector.z;  //No angles lower < 0
        //    rotation_angles.vector.z= 2 * M_PI - rotation_angles.vector.z;
    }
}

#if ERROR_IN_SIM == 1
void DroneLaserBasedPoseEstimationROSModule::droneModelStatesCallback(gazebo_msgs::ModelStatesConstPtr msg){
    /// Variables declaration
    ///
    gt_x = msg -> pose[2].position.x;
    gt_y = msg -> pose[2].position.y;
}
#endif

void DroneLaserBasedPoseEstimationROSModule::close()
{
    DroneModule::close();



    //Do stuff

    return;
}


bool DroneLaserBasedPoseEstimationROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneLaserBasedPoseEstimationROSModule::startVal()
{
    //Do stuff

    //End
    return DroneModule::startVal();
}


bool DroneLaserBasedPoseEstimationROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool DroneLaserBasedPoseEstimationROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}






